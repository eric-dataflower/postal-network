import dataclasses
import os
import os.path
import json
from redis import Redis

import requests
from typing import Tuple, Optional


directory = os.path.dirname(os.path.realpath(__file__))


REDIS_HOST = os.getenv("REDIS_HOST", "localhost")
REDIS_PORT = os.getenv("REDIS_PORT", "6379")
OSRM_HOST = os.getenv("OSRM_HOST", "127.0.0.1")
OSRM_PORT = os.getenv("OSRM_HOST", "5000")


@dataclasses.dataclass
class OSRMRouted:
    host = f"http://${OSRM_HOST}:${OSRM_PORT}"
    redis: Redis = dataclasses.field(
        default_factory=lambda: Redis(host=REDIS_HOST, port=REDIS_PORT, db=0))

    def driving(self, origin: Tuple[float, float], destination: Tuple[float, float]):
        (lat1, lon1) = origin
        (lat2, lon2) = destination
        url = self.host + f"/route/v1/driving/{lon1},{lat1};{lon2},{lat2}?geometries=geojson"
        
        if self.redis.get(url):
            return json.loads(self.redis.get(url).decode('utf-8'))
        
        response = requests.get(url)
        self.redis.set(url, response.content)
        data = response.json()
        return data

