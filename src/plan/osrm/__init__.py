import dataclasses
from .connections import OSRMRouted
import sys
import re
import json
from typing import Tuple


@dataclasses.dataclass
class RoutingMachine:
    osrm_routed: OSRMRouted = dataclasses.field(default_factory=OSRMRouted)
    _cache: dict = dataclasses.field(default_factory=dict)

    def get_route(self, origin: Tuple[float, float], destination: Tuple[float, float]):
        if not self._cache:
            print("Restoring driving cache.")
            keys = self.osrm_routed.redis.keys("*")
            for i, (key, value) in enumerate(zip(keys, self.osrm_routed.redis.mget(keys))):
                if i % 100 == 0:
                    sys.stdout.write(f"\r{i}/{len(keys)}")
                search = re.search(r"([-]?\d+[.]?[-]?\d*),([-]?\d+[.]?[-]?\d*);([-]?\d+[.]?[-]?\d*),([-]?\d+[.]?[-]?\d*)", key.decode('utf-8'))
                if search:
                    lon1, lat1, lon2, lat2 = search.groups()
                    self._cache[(float(lat1), float(lon1)), (float(lat2),
                        float(lon2))] = json.loads(value)['routes'][0]
            print()

        if (origin, destination) not in self._cache:
            data = self.osrm_routed.driving(origin, destination)
            assert data['code'] == 'Ok'
            route = data['routes'][0]
            self._cache[origin, destination] = route
        return self._cache[origin, destination]
