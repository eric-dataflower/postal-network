import json
import os
import os.path
from plan.plan import plan_network
from plan.serializers import network_to_geojson


directory = os.path.dirname(os.path.realpath(__file__))

OUTPUT_DIR = os.getenv("OUTPUT_DIR", os.path.join(directory, "output"))

if __name__ == "__main__":
    output_directory = os.path.join(OUTPUT_DIR,
        "frames")

    try:
        os.mkdir(output_directory)
    except FileExistsError:
        pass

    for i, (_, network, cost) in enumerate(plan_network(every=10000)):
        output = network_to_geojson(network)
        name = f"{i}.geojson"
        path = os.path.join(output_directory, name)

        print(f"Writing GeoJSON {i + 1}. Cost: {cost}")

        with open(path, "w") as f:
            f.write(
                json.dumps(
                    output,
                    indent=2,
                )
            )
