import csv
import os.path

directory = os.path.dirname(os.path.realpath(__file__))

post_offices = {}
post_boxes = {}

with open(os.path.join(directory, "ap.csv")) as f:
    reader = csv.reader(f)
    next(reader)
    for location_id, ap_type, lat, lon in reader:
        if ap_type == 'PO':
            post_offices[location_id] = (float(lat), float(lon))
        else:
            post_boxes[location_id] = (float(lat), float(lon))

