import csv
import os.path

directory = os.path.dirname(os.path.realpath(__file__))

lga_offices = {}

with open(os.path.join(directory, "lg.csv")) as f:
    reader = csv.reader(f)
    next(reader)
    for post_code, lat, lon in reader:
        lga_offices[post_code] = (float(lat), float(lon))

