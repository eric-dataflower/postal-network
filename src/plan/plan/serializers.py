import json
from typing import List, Tuple

import geojson
from geojson import Point, LineString

from plan.types import Network, Route, Coordinate
from data.ap import post_boxes, post_offices
from data.lg import lga_offices

from osrm import RoutingMachine


routing_machine = RoutingMachine()


def get_distance(a: Coordinate, b: Coordinate) -> float:
    return routing_machine.get_route(a, b)['distance'] / 1000.0


def haversine(a: Coordinate, b: Coordinate) -> float:
    lat1, lon1 = a
    lat2, lon2 = b
    R = 6373.0
    lon1 = math.radians(lon1)
    lat1 = math.radians(lat1)
    lon2 = math.radians(lon2)
    lat2 = math.radians(lat2)
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    _a = (
        math.sin(dlat / 2) ** 2
        + math.cos(lat1) * math.cos(lat2) * math.sin(dlon / 2) ** 2
    )
    c = 2 * math.atan2(math.sqrt(_a), math.sqrt(1 - _a))
    return R * c


def get_coordinates(route: Route, skip_empty: bool=True) -> List[Tuple[float, float]]:
    coordinates = []
    for i in range(0, len(route)):
        if i == 0 or i == len(route) - 1:
            coordinates.append(lga_offices[route[i]])
        elif route[i]:
            if route[i].endswith("_PO"):
                coordinates.append(post_offices[route[i]])
            else:
                coordinates.append(post_boxes[route[i]])
        elif not skip_empty:
            coordinates.append(None)
    return coordinates


def get_distances(coordinates: List[Tuple[float, float]]) -> float:
    start = coordinates[0]
    distances = []
    for coordinate in coordinates[1:]:
        distances.append(get_distance(start, coordinate))
        start = coordinate
    return distances


def network_to_geojson(state: Network) -> geojson.FeatureCollection:    
    features = []

    for lga_office, (lat, lon) in lga_offices.items():
        features.append(geojson.Feature(
            geometry=Point((lon, lat)), properties={
                'name': lga_office,
                "marker-color": "#060",
                "marker-symbol": "city",
                "marker-size": "medium",
            }))

    for post_office, (lat, lon) in post_offices.items():
        features.append(geojson.Feature(
            geometry=Point((lon, lat)), properties={
                'name': lga_office,
                "marker-color": "#900",
                "marker-symbol": "warehouse",
                "marker-size": "medium",
            }))

    for route in state:

        coordinates = get_coordinates(route, skip_empty=False)
        stops = list(zip(route, coordinates))
        
        start = stops[0]
        for i, stop in enumerate(stops[1:]):
            (lat1, lon1) = start[1]

            if not stop[0]:
                continue

            (lat2, lon2) = stop[1]

            if stop[0] and not stop[0].endswith("_PO") and i != len(stops) - 2:
                features.append(geojson.Feature(
                    geometry=Point((lon2, lat2)), properties={
                        'name': lga_office,
                        "marker-color": "#009",
                        "marker-symbol": "cemetery",
                        "marker-size": "medium",
                    }))

            # if haversine(start[1], stop[1]) < 10.0:
            #     start = stop
            #     continue

            geometry = routing_machine.get_route(start[1], stop[1])['geometry']

            features.append(geojson.Feature(geometry=geometry))
            features.append(geojson.Feature(
                geometry=LineString(
                    [(lon1, lat1), (lon2, lat2)]
                ),
                properties={
                    'stroke-color': '#606',
                    'stroke-width': '1'
                }))

            start = stop
    
    # for post_box, (lat, lon) in post_boxes.items():
    #     features.append(geojson.Feature(
    #         geometry=Point((lon, lat)), properties={
    #             'name': lga_office,
    #             "marker-color": "#300",
    #             "marker-symbol": "cemetery",
    #             "marker-size": "small",
    #         }))

    return geojson.FeatureCollection(features)

