import math
import random
from typing import Tuple, List, Generator

import geojson
from simulated_annealing import anneal
from data.ap import post_boxes, post_offices
from data.lg import lga_offices
from plan.types import Network, Route, Coordinate
from plan.serializers import get_coordinates, get_distances


lga_office_list = list(lga_offices)



def random_network(use_post_boxes) -> Network:
    print("Number of post offices:", len(post_offices))
    print("Number of post boxes:", len(post_boxes))

    _post_offices = list(post_offices)

    random.shuffle(_post_offices)

    network = []
    while _post_offices:
        lga1 = random.choice(lga_office_list)
        lga2 = lga1

        while lga2 == lga1:
            lga2 = random.choice(lga_office_list)
    
        route = [lga1, None, None, None, None, lga2]

        for i in range(1, 5):
            if not _post_offices:
                break

            po = _post_offices.pop()
            route[i] = po

        network.append(route)

    return network


def get_route_cost(route: Route, debug=False) -> float:
    cost = 0.0
    distances = get_distances(get_coordinates(route))
    for distance in distances:
        if distance <= 10.0:
            cost += 1000.0 + distance * 100
        elif distance <= 40.0:
            cost += 4000.0 + distance * 100
        else:
            cost += 40000 + distance * 100
    cost += (len(distances) - 1) * 1000.0
    if debug:
        print(route, distances, cost)
    return cost


def get_cost(state: Network, debug: bool=False) -> float:
    cost = 0.0

    for route in state:
        cost += get_route_cost(route, debug=debug)

    return cost


def get_route_neighbor_and_cost(route: Route, cost: float):
    route = list(route)
    i = random.randint(0, len(route) - 1)
    if i == 0 or i == len(route) - 1:
        route[i], route[-1] = route[-1], route[i]
    else:
        j = random.randint(1, len(route) - 2)
        route[i], route[j] = route[j], route[i]
    return tuple(route), get_route_cost(route)


def get_neighbor_and_cost(state: Network, cost: float) -> Tuple[Network, float]:
    network = state.copy()
    a = list(network.pop(random.randrange(len(network))))
    start_a_cost = get_route_cost(a)
    i = random.randint(0, len(a) - 1)
    if i == 0 or i == len(a) - 1:
        lga2 = a[len(a) - 1 - i]
        a[i] = lga2
        while a[i] == lga2:
            a[i] = random.choice(lga_office_list)
        network.append(tuple(a))
        return network, cost - start_a_cost + get_route_cost(a)
    else:
        b = list(network.pop(random.randrange(len(network))))
        j = random.randint(1, len(b) - 2)
        start_b_cost = get_route_cost(b)
        a[i], b[j] = b[j], a[i]
        network.append(tuple(a))
        network.append(tuple(b))
        return network, (cost - start_a_cost - start_b_cost +
            get_route_cost(a) + get_route_cost(b))


def plan_network(use_post_boxes: int = 0, every: int = 0) -> Generator[Network, float, None]:
    start_state = random_network(use_post_boxes=100)
    start_cost = get_cost(start_state)
    total_steps = 0
    outputs = []
    
    print("Start cost: ", start_cost)

    for i in range(0, 10):
        print("===", i, "===")
        step, output_state, output_cost = yield from anneal(
            start_state,
            start_cost,
            get_neighbor_and_cost,
            maximum_steps=100000,
            every=every,
            get_temperature = lambda a, b: math.sqrt((1 - a / b)) * 1250)
        start_state = output_state
        start_cost = output_cost
        print("Cost: ", output_cost)
        total_steps += step

    print("===", "main", "===")

    step, output_state, output_cost = yield from anneal(
        start_state,
        start_cost,
        get_neighbor_and_cost,
        maximum_steps=1000000,
        every=every,
        get_temperature = lambda a, b: math.sqrt((1 - a / b)) * 1250)
    total_steps += step

    print("Cost: ", output_cost)

    print("===", "local", "===")
    start_state = output_state
    output_state = []
    while start_state:
        route = start_state.pop()
        step, route, _ = yield from anneal(
            route,
            get_route_cost(route),
            get_route_neighbor_and_cost,
            maximum_steps=1000,
            get_temperature = lambda a, b: math.sqrt((1 - a / b)))
        output_state.append(route)
    
    print("Output: ", output_state)
    print("Cost: ", get_cost(output_state, debug=True))
    outputs.append(output_state)
    yield total_steps, output_state, output_cost
