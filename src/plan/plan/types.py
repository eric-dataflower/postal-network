from typing import Tuple, List, Optional, Set

OfficeID = str
LocationID = str
Route = Tuple[
    OfficeID,
    LocationID,
    LocationID,
    Optional[LocationID],
    Optional[LocationID],
    OfficeID
]
Network = List[Route]

Coordinate = Tuple[float, float]
