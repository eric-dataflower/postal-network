import os
import os.path


directory = os.path.dirname(os.path.realpath(__file__))

FRAMES_DIR = os.getenv("FRAMES_DIR", os.path.join(directory, "frames"))

if __name__ == "__main__":
    frames = []

    for name in os.listdir(FRAMES_DIR):
        if name.endswith(".geojson"):
            index, extension = name.split(".")
            frames.append(int(index))

    frames.sort()

    for index in frames:
        name = str(index) + ".geojson"
        with open(os.path.join(FRAMES_DIR, name)) as f:
            pass
