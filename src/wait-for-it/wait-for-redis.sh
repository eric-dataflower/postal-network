#!/bin/sh
# wait-for-redis.sh

set -e
  
host="$1"
shift
cmd="$@"
  
until python -c "from redis import Redis;Redis(host=\"$host\", db=0).get(\"a\")"; do
  >&2 echo "Redis is unavailable - sleeping"
  sleep 1
done
  
>&2 echo "Redis is up - executing command"
exec $cmd
