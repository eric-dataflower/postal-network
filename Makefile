.PHONY: data build run src

# Render the GeoJSON snapshots as an animation.
./output/render: ./output/frames
	docker-compose run render

# Run the planning process, output progress as a set of GeoJSON snapshots.
./output/frames:
	docker-compose run plan

# Build docker images.
build: data src
	docker-compose build

# Download required data.
data:
	make -C data

# Run requisite script for docker build command.
src:
	make -C src/plan
