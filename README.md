# Postal Network

## Challenge

Australia Post is setting up a new delivery network in the Sydney Metropolitan
Area, with the following requirements:

1. Every post office must be visited.
2. Each delivery path starts at a local government council office,
   visits 4 post office or boxes and arrives at another local government
   council office.
3. The delivery path does not intersect itself.
4. Each leg between locations:
   - If less than a constant X KM distance by road, costs $1000 per year.
   - Otherwise, it, costs $4000 per year.
   - Maximum distance between two delivery locations is Y.
5. Each Post office or post box visited costs $1000 per year to maintain.
6. While it is mandatory to visit every post office, post boxes are not required
   to be visited - unless it helps with reducing the network's operational cost.
7. Produce an exception report for post offices that cannot be visited.

Plan the network with an approximately minimal cost.

## Data

- [Sydney Australia Post offices and boxes](src/plan/data/ap.csv)
- [Sydney local government offices](src/plan/data/lg.csv)

## Quickstart

1. Docker requires about 4GB of RAM and 4GB of Swap to run the following
   commands. You can configure this at:
   > Docker Desktop > Settings > Resources > Advanced.
2. Run the following, takes a few hours:

```
# Download and prepare prerequisite data.
make data

# Build docker images.
make build

# Plan postal network, output progress in GeoJSON format to ./output/frames.
make output/frames

# Render the GeoJSON snapshots as an animation to ./output/render.
make output/render
```

## Credits

- This project is authored by [Eric Man](mailto:eric@dataflower.com.au)
- Depends on ambitious open source efforts of:
  - [OSRM](http://project-osrm.org), for the C++ routing engine.
  - [Mapnik](mapnik.org), for map and GeoJSON rendering.
  - [OpenStreetMaps](https://www.openstreetmap.org), for roads data.
  - [geofabrik](https://www.geofabrik.de), for packaging OSM data.
- Sydney post office and boxes data from:
  - [Australia Post](auspost.com.au)

## License

- This software is licensed under [AGPLv3](LICENSE.md).
